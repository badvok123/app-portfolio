package com.example.badvok.appportfolio;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind (R.id.menu_button_1)
    Button btnSpotifyStreamer;

    @Bind (R.id.menu_button_2)
    Button btnScoresApp;

    @Bind (R.id.menu_button_3)
    Button btnLibraryApp;

    @Bind (R.id.menu_button_4)
    Button btnBuildItBigger;

    @Bind (R.id.menu_button_5)
    Button btnXyzReader;

    @Bind (R.id.menu_button_6)
    Button btnMyOwnApp;

    String thisWillLaunch = "This button will launch my";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @OnClick(R.id.menu_button_1)
    public void launchSpotifyStreamer() {
       buttonToast(getString(R.string.menu_button_1)).show();
        Log.d("asd","asdasdasdasda");
    }
    @OnClick(R.id.menu_button_2)
    public void launchScoresApp() {
        buttonToast(getString(R.string.menu_button_2)).show();

    }
    @OnClick(R.id.menu_button_3)
    public void launchLibraryApp() {
        buttonToast(getString(R.string.menu_button_3)).show();

    }
    @OnClick(R.id.menu_button_4)
    public void launchBuildItBigger() {
        buttonToast(getString(R.string.menu_button_4)).show();

    }
    @OnClick(R.id.menu_button_5)
    public void launchXyzReaderr() {
        buttonToast(getString(R.string.menu_button_5)).show();

    }
    @OnClick(R.id.menu_button_6)
    public void launchMyOwnApp() {
        buttonToast(getString(R.string.menu_button_6)).show();

    }

    public Toast buttonToast(String btnName){
        return Toast.makeText(getApplicationContext(),thisWillLaunch + btnName,Toast.LENGTH_LONG);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
